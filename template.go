package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"text/template"
)

var keystore = make(map[string]interface{})
var templates = make(map[string]*template.Template)
var FMap template.FuncMap = map[string]interface{}{
	"getlinks": GetLinks,
	"isset":    IsSet,
	"index":    Index,
	"join":     strings.Join,
	"title":    strings.Title,
}

type Links struct {
	Link        string
	Url         string
	Description []string
}

func Index(a interface{}, field string) (interface{}, error) {
	if ok, err := IsSet(a, field); !ok {
		if err != nil {
			return nil, err
		}
		return nil, errors.New("field " + field + " not set")
	}
	v := reflect.ValueOf(a).FieldByNameFunc(func(name string) bool {
		if strings.ToLower(name) == strings.ToLower(field) {
			return true
		}
		return false
	})
	return v.Interface(), nil
}

func IsSet(a interface{}, field string) (bool, error) {
	rStruct := reflect.TypeOf(a)
	if rStruct.Kind() != reflect.Struct {
		return false, errors.New("isset called on non-struct")
	}
	_, ok := rStruct.FieldByNameFunc(func(name string) bool {
		if strings.ToLower(name) == strings.ToLower(field) {
			return true
		}
		return false
	})
	return ok, nil
}

func GetLinks(srcFile string) ([]Links, error) {
	content, err := ioutil.ReadFile(srcFile)
	if err != nil {
		return nil, fmt.Errorf("unable to read file %q: %v", srcFile, err)
	}
	var linkList []Links
	if err := json.Unmarshal(content, &linkList); err != nil {
		return nil, fmt.Errorf("unable to decode json in file %q: %v", srcFile, err)
	}
	return linkList, nil
}

func storeTemplates(src string) error {
	dir, err := os.Open(src)
	if err != nil {
		return fmt.Errorf("Unable to open dir %q: %v", src, err)
	}
	objects, err := dir.Readdir(-1)
	if err != nil {
		return fmt.Errorf("Unable to read dir %q: %v", src, err)
	}
	for _, obj := range objects {
		srcPath := filepath.Join(src, obj.Name())
		if obj.IsDir() {
			if err = storeTemplates(srcPath); err != nil {
				return err
			}
			continue
		}
		data, err := ioutil.ReadFile(srcPath)
		if err != nil {
			return err
		}
		if templates[obj.Name()], err = template.New(obj.Name()).Funcs(FMap).Parse(string(data)); err != nil {
			return fmt.Errorf("reading file %q failed with: %v", err)
		}
	}
	return nil
}
