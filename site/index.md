---
template: default.html
---
This website is an experimentation in accessibility and reliability. By setting
up redirects from this website's short urls to things I would like to share
with others, mostly links from my blog, I want to see if it would be easier to
make my content more accessbile and more memorable.

In case you were looking for the main blog, head over to
<https://www.varstack.com>.

