package main

import (
	"log"
	"os"
)

func main() {
	if err := os.RemoveAll("public"); err != nil {
		log.Fatalln("Unable to remove dir public:", err)
	}
	if err := transformDir("site", "public"); err != nil {
		log.Fatalln("Directory transformation failed with:", err)
	}
}
