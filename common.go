package main

import (
	"io"
	"os"
	"path/filepath"
	"sort"
)

func sortDirList(objects []os.FileInfo) {
	sort.Slice(objects, func(i, j int) bool {
		name1 := objects[i].Name()
		name2 := objects[j].Name()
		if name1[0] == '_' && name2[0] != '_' {
			return true
		}
		if name1[0] != '_' && name2[0] == '_' {
			return false
		}
		return name1 < name2
	})
}

func setupAndListDir(src, dst string) ([]os.FileInfo, error) {
	stat, err := os.Stat(src)
	if err != nil {
		return nil, err
	}
	if err = os.MkdirAll(dst, stat.Mode()); err != nil {
		return nil, err
	}
	dir, err := os.Open(src)
	if err != nil {
		return nil, err
	}
	return dir.Readdir(-1)
}

func transformDir(src, dst string) error {
	objects, err := setupAndListDir(src, dst)
	if err != nil {
		return err
	}
	sortDirList(objects)
	for _, obj := range objects {
		srcPath := filepath.Join(src, obj.Name())
		dstPath := filepath.Join(dst, obj.Name())
		if obj.IsDir() {
			switch obj.Name() {
			case "_template":
				err = storeTemplates(srcPath)
			default:
				err = transformDir(srcPath, dstPath)
			}
		} else {
			switch filepath.Ext(srcPath) {
			case ".md":
				err = markdownTransform(srcPath, dstPath)
			case ".ts":
				err = typescriptCompile(srcPath, dstPath)
			case ".json":
				err = linkGenerator(srcPath, dst)
			default:
				err = copyFile(srcPath, dstPath)
			}
		}
		if err != nil {
			return err
		}
	}
	return nil
}

func copyFile(src string, dst string) (err error) {
	sourcefile, err := os.Open(src)
	if err != nil {
		return err
	}
	defer sourcefile.Close()
	destfile, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destfile.Close()
	if _, err = io.Copy(destfile, sourcefile); err != nil {
		return err
	}
	if stat, err := os.Stat(src); err == nil {
		_ = os.Chmod(dst, stat.Mode())
	}
	return nil
}
