package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"reflect"
	"text/template"

	"github.com/russross/blackfriday"
	"gopkg.in/yaml.v2"
)

func getFrontMatter(src string, txt []byte) (map[string]interface{}, []byte, error) {
	splitText := bytes.Split(txt, []byte{'\n'})
	if !reflect.DeepEqual(splitText[0], []byte("---")) {
		log.Println("Front matter not found in file:", src)
		return nil, txt, nil
	}
	splitText = splitText[1:]
	yamlText := make([]byte, 0, 500)
	for i, line := range splitText {
		if reflect.DeepEqual(line, []byte("---")) {
			conf := make(map[string]interface{})
			if err := yaml.Unmarshal(yamlText, &conf); err != nil {
				return nil, nil, err
			}
			return conf, bytes.Join(splitText[i+1:], []byte("\n")), nil
		}
		yamlText = append(yamlText, '\n')
		yamlText = append(yamlText, line...)
	}
	log.Println("Front matter not found in file:", src)
	return nil, txt, nil
}

func markdownTransform(src, dst string) error {
	dst = dst[:len(dst)-len(filepath.Ext(src))] + ".html"
	txt, err := ioutil.ReadFile(src)
	if err != nil {
		return err
	}

	config, txt, err := getFrontMatter(src, txt)
	if err != nil {
		return err
	}
	if txt, err = applyTemplating(txt, config); err != nil {
		return err
	}
	html := blackfriday.MarkdownCommon(txt)
	output := []byte{}
	lines := bytes.Split(html, []byte{'\n'})
	for _, line := range lines {
		output = append(output, line...)
		output = append(output, '\n')
	}

	output, err = preprocess(output, config["template"])
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(dst, output, 0644)
	return err
}

func typescriptCompile(src, dst string) error {
	dst = dst[:len(dst)-len(filepath.Ext(src))] + ".js"
	cmd := exec.Command("tsc", src, "--out", dst)
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf(
			"command `tsc` failed on paths (%q -> %q): %v", src, dst, err)
	}
	return nil
}

func preprocess(text []byte, conf interface{}) ([]byte, error) {
	if conf == nil {
		conf = "default.html"
	}
	c, ok := conf.(string)
	if !ok {
		return nil, fmt.Errorf(
			"`template` in front-matter is %T instead of string", conf)
	}
	template, ok := templates[c]
	if !ok {
		return nil, fmt.Errorf("template %q not found", template)
	}
	buf := new(bytes.Buffer)
	if err := template.Execute(buf, map[string]interface{}{
		"content": string(text),
	}); err != nil {
		return nil, fmt.Errorf("applying template failed: %v", err)
	}
	return buf.Bytes(), nil
}

func applyTemplating(text []byte, templ map[string]interface{}) ([]byte, error) {
	t, err := template.New("md").Funcs(FMap).Parse(string(text))
	if err != nil {
		return nil, err
	}
	buf := new(bytes.Buffer)
	if err := t.Execute(buf, templ); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func linkGenerator(srcFile, dstDir string) error {
	linkList, err := GetLinks(srcFile)
	if err != nil {
		return err
	}
	for _, l := range linkList {
		path := filepath.Join(dstDir, l.Link)
		fname := filepath.Join(path, "index.html")
		redirectText := `<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="application/pdf" />
		<meta http-equiv="refresh" content="0; url=` + l.Url + `" />
	</head>
</html>`
		if err := os.Mkdir(path, 0755); err != nil {
			return fmt.Errorf("unable to create dir %q: %v", path, err)
		}
		if err := ioutil.WriteFile(fname, []byte(redirectText), 0644); err != nil {
			return fmt.Errorf("unable to write file %q: %v", path, err)
		}
	}
	return nil
}
